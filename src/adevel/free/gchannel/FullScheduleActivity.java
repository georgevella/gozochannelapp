package adevel.free.gchannel;

import adevel.free.gchannel.adapters.FullScheduleAdapter;
import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;

public class FullScheduleActivity extends ListActivity {
	
	private static final String TAG = "FullScheduleActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) { 
		super.onCreate(savedInstanceState);			
		
		Log.d(TAG, "Initializing FullScheduleActivity");
		
		Bundle intentBundle = getIntent().getExtras();
		String harbourId = intentBundle.getString(BundleProps.OriginHarbour);
		
		FullScheduleAdapter adapter = new FullScheduleAdapter(this);
		adapter.setDepartureHarbour(harbourId);
		adapter.refreshItems();
		setListAdapter(adapter);
		
		Log.d(TAG, "Initializing FullScheduleActivity ... ok");
	}
}
