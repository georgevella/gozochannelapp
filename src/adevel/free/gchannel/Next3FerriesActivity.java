package adevel.free.gchannel;

import java.util.Iterator;

import adevel.free.gchannel.adapters.NextXFerriesScheduleAdapter;
import adevel.free.gchannel.model.ScheduleStore;
import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class Next3FerriesActivity extends Activity {
    private ArrayAdapter<HarbourInfo> _harbourAdapter;
	private static final String TAG = "Next3FerriesActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
		
		Log.d(TAG, "Initializing Next3FerriesActivity");
		
		setContentView(R.layout.upcoming_ferries);

        // setup departures / harbours adapter 
        _harbourAdapter = new ArrayAdapter<HarbourInfo>(this, android.R.layout.simple_spinner_item);        
        _harbourAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        
        // refresh the harbours adapter
        refreshHarbours();
        
        // setup departures spinner
        Log.d(TAG, "Initializing the origin harbour spinner");
        Spinner departuresList = (Spinner) findViewById(R.id.departureHarbour);
        departuresList.setAdapter(_harbourAdapter);
        departuresList.setOnItemSelectedListener(new DepartureHarbourChangeListener(this));
        
        // update schedule adapters with the currently selected harbour
        Log.d(TAG, "Updating schedule adapters with the currently selected origin harbour");
        HarbourInfo hInfo = (HarbourInfo) departuresList.getSelectedItem();
        sendBroadcast(new Intent().setAction(IntentActions.RefreshSchedule)
        		.putExtra(BundleProps.OriginHarbour, hInfo.getId()));
        
        // setup listview
        ListView list = (ListView)findViewById(R.id.upcomingferrieslist);
        list.setAdapter(NextXFerriesScheduleAdapter.INSTANCE);
        
		
		Log.d(TAG, "Initializing Next3FerriesActivity ... ok");
	}
	
	private void refreshHarbours() {
    	Log.d(TAG, "refreshHarbours >> ");
    	_harbourAdapter.clear();
    	
    	Iterator<HarbourInfo> hInfoIterator = ScheduleStore.INSTANCE.getHarbours().iterator();
        while (hInfoIterator.hasNext())
        {
        	_harbourAdapter.add(hInfoIterator.next());
        }
        Log.d(TAG, "refreshHarbours << ");
	}

	private class DepartureHarbourChangeListener implements OnItemSelectedListener {
    	private static final String TAG = "DepartureHarbourChange";

		public DepartureHarbourChangeListener(Context context) {			
		}

		public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {			
			Log.d(TAG, "onItemSelected >> "+parent.getClass().toString() + "," + pos + "," + id);
			
			HarbourInfo hInfo = (HarbourInfo)parent.getItemAtPosition(pos);
			
			Log.d(TAG, "Harbour ID selected ["+hInfo.getId()+"]");
	        
			NextXFerriesScheduleAdapter.INSTANCE.setDepartureHarbour(hInfo.getId());
			NextXFerriesScheduleAdapter.INSTANCE.refreshItems();
	        
			Log.d(TAG, "onItemSelected <<");
		}

		public void onNothingSelected(AdapterView<?> arg0) {
			Log.d(TAG, "OnNothingSelected >>");
			Log.d(TAG, "OnNothingSelected <<");
			
		}
    }
}
