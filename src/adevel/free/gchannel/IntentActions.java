package adevel.free.gchannel;

public class IntentActions {
	static public final String RefreshSchedule = "adevel.free.gchannel.REFRESH_SCHEDULE";
	static public final String ShowNextFerries = "adevel.free.gchannel.SHOW_NEXT_FERRIES";
	static public final String ShowFullSchedule = "adevel.free.gchannel.SHOW_FULL_SCHEDULE";
}
