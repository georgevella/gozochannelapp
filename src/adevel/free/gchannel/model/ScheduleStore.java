package adevel.free.gchannel.model;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import adevel.free.gchannel.HarbourInfo;
import adevel.free.gchannel.R;
import android.content.Context;

public class ScheduleStore 
{
	public static final ScheduleStore INSTANCE = new ScheduleStore();
	
	///////////////////////////////////////////////////////////////
	public ScheduleStore() { }
	
	private ArrayList<ScheduleEntry> _items = new ArrayList<ScheduleEntry>();
	private ArrayList<HarbourInfo> _harbours = new ArrayList<HarbourInfo>();
	
	public void Initialize(Context c)
	{
		loadData(c);
	}
	
	private void loadData(Context c)
	{
		_items.clear();
		_harbours.clear();
		
		Document doc = null;
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {

			DocumentBuilder db = dbf.newDocumentBuilder();
	
			InputSource is = new InputSource();
	        is.setByteStream(c.getResources().openRawResource(R.raw.timetable));
	        doc = db.parse(is); 
	        
	        NodeList nodes = doc.getElementsByTagName("entry");
	        
	        for (int i = 0; i<nodes.getLength(); i++)
	        {
	        	Element e = (Element)nodes.item(i);
	        	_items.add(ScheduleEntry.ParseElement(e));
	        }
	        
	        nodes = doc.getElementsByTagName("harbour");
	        for (int i = 0; i<nodes.getLength(); i++)
	        {
	        	Element e = (Element)nodes.item(i);
	        	_harbours.add(HarbourInfo.ParseElement(e));
	        }

		} catch (ParserConfigurationException e) {
			System.out.println("XML parse error: " + e.getMessage());
		} catch (SAXException e) {
			System.out.println("Wrong XML file structure: " + e.getMessage());
		} catch (IOException e) {
			System.out.println("I/O exeption: " + e.getMessage());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			System.out.println("ParseException "+ e.getMessage());
		}
	}

	public List<ScheduleEntry> getAll() {
		return _items;
		
	}

	public List<HarbourInfo> getHarbours() {
		return _harbours;
	}

}
