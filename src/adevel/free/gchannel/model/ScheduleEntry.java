package adevel.free.gchannel.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumSet;

import org.w3c.dom.Element;

public class ScheduleEntry {
	
//	public static EnumSet<ScheduleDay> Daily = EnumSet.of(ScheduleDay.Monday, ScheduleDay.Tuesday, ScheduleDay.Wednesday, ScheduleDay.Thursday, 
//			ScheduleDay.Friday, ScheduleDay.Saturday, ScheduleDay.Sunday );
	
	public static EnumSet<ScheduleDay> Daily = EnumSet.allOf(ScheduleDay.class);

	public static ScheduleEntry ParseElement(Element e) throws ParseException {
		ScheduleEntry se = new ScheduleEntry();		
		
		se.setDepartureTime(new SimpleDateFormat("HH:mm").parse(e.getAttribute("time")));
		se.setDepartureHarbourId(e.getAttribute("origin"));
		
		se.setDestinationHarbourId(e.getAttribute("dest"));
		
		if (e.hasAttribute("daily"))
		{
			if (e.getAttribute("daily").compareToIgnoreCase("true") == 0)
			{
				se._days = Daily;
			}
			else 
			{
				if (e.hasAttribute("days"))
				{
					se._days = EnumSet.noneOf(ScheduleDay.class);
					
					String[] rawDays = e.getAttribute("days").split(",");
					for (String rDay : rawDays)
					{
						if (rDay.compareToIgnoreCase("mon") == 0)
						{
							se._days.add(ScheduleDay.Monday);
						}
						else if (rDay.compareToIgnoreCase("tue") == 0)
						{
							se._days.add(ScheduleDay.Tuesday);
						}
						else if (rDay.compareToIgnoreCase("wed") == 0)
						{
							se._days.add(ScheduleDay.Wednesday);
						}
						else if (rDay.compareToIgnoreCase("thur") == 0)
						{
							se._days.add(ScheduleDay.Thursday);
						}
						else if (rDay.compareToIgnoreCase("fri") == 0)
						{
							se._days.add(ScheduleDay.Friday);
						}
						else if (rDay.compareToIgnoreCase("sat") == 0)
						{
							se._days.add(ScheduleDay.Saturday);
						}
						else if (rDay.compareToIgnoreCase("sun") == 0)
						{
							se._days.add(ScheduleDay.Sunday);
						}
					}
				}
				else
				{
					se._days = Daily;
				}
			}
		}
		else
		{
			se._days = Daily;
		}
		
		if (e.hasAttribute("includePublicHolidays"))
		{
			se._includePublicHolidays = (e.getAttribute("includePublicHolidays").equalsIgnoreCase("true"));
		}
		
		
		return se;
	}

	private boolean _includePublicHolidays;
	private String _destHarbourId;
	private Date _departureTime;
	private String _departureHarbourId;
	private ScheduleDay _scheduleDay;
	private EnumSet<ScheduleDay> _days;

	private void setDestinationHarbourId(String attribute) {
		_destHarbourId = attribute;
	}
	
	public String getDestinationHarbourId()
	{
		return _destHarbourId;
	}
	
	public boolean isDaily()
	{
		return _days == Daily;
	}

	public boolean availableOnPublicHolidays()
	{
		return _includePublicHolidays;
	}

	private void setDepartureHarbourId(String attribute) {
		_departureHarbourId = attribute;
		
	}
	
	public String getDepartureHarbourId()
	{
		return _departureHarbourId;
	}

	private void setDepartureTime(Date parse) {
		_departureTime = parse;
	}
	
	public Date getDepartureTime()
	{	
		Calendar c = Calendar.getInstance();
		Calendar now = Calendar.getInstance();
		
		int hour = _departureTime.getHours();
		c.set(Calendar.HOUR_OF_DAY, hour);
		
		int min = _departureTime.getMinutes();
		c.set(Calendar.MINUTE, min);
		c.set(Calendar.SECOND, 0);
		
		if (c.before(now))
		{
			c.add(Calendar.DATE, 1);
		}
		
		return c.getTime();
	}
	
	public String getDepartureTimeString()
	{
		return new SimpleDateFormat("HH:mm").format(_departureTime);
	}

	public ScheduleDay getScheduleDay() {
		return _scheduleDay;
	}

	public void setScheduleDay(ScheduleDay _scheduleDay) {
		this._scheduleDay = _scheduleDay;
	}

}
