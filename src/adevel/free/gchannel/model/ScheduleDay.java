package adevel.free.gchannel.model;

public enum ScheduleDay {
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday
}
