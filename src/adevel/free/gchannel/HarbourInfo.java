package adevel.free.gchannel;

import org.w3c.dom.Element;

public class HarbourInfo {

	public static HarbourInfo ParseElement(Element e) {
		HarbourInfo hi = new HarbourInfo();
		
		hi.setDisplayName(e.getAttribute("displayName"));
		hi.setId(e.getAttribute("id"));
		
		return hi;
	}

	private String _displayName;
	private String _id;

	private void setId(String attribute) {
		_id = attribute;
		
	}
	
	public String getId()
	{
		return _id;
	}

	public String getDisplayName() {
		return _displayName;
	}

	public void setDisplayName(String displayName) {
		_displayName = displayName;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getDisplayName();
	}
	

}
