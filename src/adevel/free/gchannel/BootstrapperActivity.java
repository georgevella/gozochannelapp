package adevel.free.gchannel;

import adevel.free.gchannel.R;
import adevel.free.gchannel.adapters.FullScheduleAdapter;
import adevel.free.gchannel.adapters.NextXFerriesScheduleAdapter;
import adevel.free.gchannel.model.ScheduleStore;
import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;

public class BootstrapperActivity extends TabActivity {
    private static final String TAG = "Bootstrapper";
    
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        Log.d(TAG, "onCreate() >>");
        
        setContentView(R.layout.main);
        
        // setup schedule adapters
        NextXFerriesScheduleAdapter.INSTANCE.setContext(this);
        FullScheduleAdapter.INSTANCE.setContext(this);

        // initialize store
        ScheduleStore.INSTANCE.Initialize(this);   
                
        // setup tabs
        Log.d(TAG, "Setting up TabHost ...");
        TabHost th = getTabHost();
        TabHost.TabSpec tabSpec;  
        
        tabSpec = th.newTabSpec("nextferries")
        		.setIndicator("Next Ferries")
        		.setContent(new Intent().setAction(IntentActions.ShowNextFerries));
        th.addTab(tabSpec);
        
        tabSpec = th.newTabSpec("departingCirkewwa")
        		.setIndicator("From Cirkewwa")
        		.setContent(new Intent().setAction(IntentActions.ShowFullSchedule).putExtra(BundleProps.OriginHarbour, "cirkewwa"));
        th.addTab(tabSpec);
        
        tabSpec = th.newTabSpec("departingMgarr")
        		.setIndicator("From Mgarr")
        		.setContent(new Intent().setAction(IntentActions.ShowFullSchedule).putExtra(BundleProps.OriginHarbour, "mgarr"));
        th.addTab(tabSpec);
        
        Log.d(TAG, "onCreate() <<");
        
    }

    
    
}