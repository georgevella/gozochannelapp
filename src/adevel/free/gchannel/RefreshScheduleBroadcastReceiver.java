package adevel.free.gchannel;

import adevel.free.gchannel.adapters.FullScheduleAdapter;
import adevel.free.gchannel.adapters.NextXFerriesScheduleAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class RefreshScheduleBroadcastReceiver extends BroadcastReceiver {
	
	private static final String TAG = "RefreshScheduleReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		
		Log.d(TAG, "onReceive >>");
		
		Bundle extras = intent.getExtras();
		
		String originHarbourId = extras.getString(BundleProps.OriginHarbour);
		Log.d(TAG, "Origin Harbour is ["+originHarbourId+"], creating adapter");
		
		NextXFerriesScheduleAdapter.INSTANCE.setDepartureHarbour(originHarbourId);
		NextXFerriesScheduleAdapter.INSTANCE.refreshItems();
		
		FullScheduleAdapter.INSTANCE.setDepartureHarbour(originHarbourId);
		FullScheduleAdapter.INSTANCE.refreshItems();
				
		Log.d(TAG, "onReceive <<");

	}

}
