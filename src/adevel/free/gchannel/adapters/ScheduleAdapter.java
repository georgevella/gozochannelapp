package adevel.free.gchannel.adapters;


import java.util.ArrayList;
import java.util.List;

import adevel.free.gchannel.R;
import adevel.free.gchannel.model.ScheduleEntry;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public abstract class ScheduleAdapter extends BaseAdapter {
	
	private Context _context;
	
	private ArrayList<ScheduleEntry> _items;

	private String _harbourId;
	
	public ScheduleAdapter()
	{
		_items = new ArrayList<ScheduleEntry>();
	}
	
	protected List<ScheduleEntry> getItems()
	{		
		return _items;
	}
	
	public abstract void refreshItems();

	public int getCount() {
		return _items.size();
	}

	public Object getItem(int position) {
		return _items.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View v = convertView;		
        if (v == null) {
                LayoutInflater vi = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.list_item, null);
        }

        ScheduleEntry it = _items.get(position);
        if (it != null) {
//                ImageView iv = (ImageView) v.findViewById(R.id.item_image);
//                if (iv != null) {
//                	iv.setImageResource(R.drawable.ic_launcher);                               
//                }
                
                TextView tv = (TextView) v.findViewById(R.id.item_text);
                if (tv != null)
                {
                	tv.setText(it.getDepartureTimeString());
                }
        }

        return v;
	}

	public Context getContext() {
		return _context;
	}

	public void setContext(Context _context) {
		this._context = _context;
	}
	
	public void setDepartureHarbour(String harbourId)
	{
		_harbourId = harbourId;
	}
	
	public String getSelectedDepartureHarbour()
	{
		return _harbourId;
	}

}
