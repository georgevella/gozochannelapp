package adevel.free.gchannel.adapters;

import java.util.Iterator;
import java.util.List;

import adevel.free.gchannel.model.ScheduleEntry;
import adevel.free.gchannel.model.ScheduleStore;
import android.content.Context;

public class FullScheduleAdapter extends ScheduleAdapter {

	public static final FullScheduleAdapter INSTANCE = new FullScheduleAdapter();
	
	///////////////////////////////////////////////////////////////////////
	public FullScheduleAdapter()
	{
		
	}
	
	public FullScheduleAdapter(Context c)
	{
		super();
		
		setContext(c);
	}
	
	public void refreshItems()
	{
		// clear previous list
		getItems().clear();
		
		List<ScheduleEntry> schedule = ScheduleStore.INSTANCE.getAll();
		Iterator<ScheduleEntry> it = schedule.iterator();
		
		while(it.hasNext())
		{
			ScheduleEntry se = it.next();
			
			if (se.getDepartureHarbourId().equals(this.getSelectedDepartureHarbour()))
				getItems().add(se);
		}
		
		// we need to call this to notify that the data has changed
		notifyDataSetChanged();
	}

}
