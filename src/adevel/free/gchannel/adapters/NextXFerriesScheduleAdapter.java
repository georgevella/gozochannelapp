package adevel.free.gchannel.adapters;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import adevel.free.gchannel.model.ScheduleEntry;
import adevel.free.gchannel.model.ScheduleStore;
import android.util.Log;



public class NextXFerriesScheduleAdapter extends ScheduleAdapter {
	
	private static final String TAG = "NextXFerriesScheduleAdapter"; 
	
	public static final NextXFerriesScheduleAdapter INSTANCE = new NextXFerriesScheduleAdapter();
	
	/////////////////////////////////////////////////////////////////////////

	private int _maxItems;

	public NextXFerriesScheduleAdapter() {		
		super();
		
		_maxItems = 3;
	}

	public void refreshItems() 
	{	
		Log.d(TAG, "refreshItems() >>");
		
		getItems().clear();
		
		Date now = new Date();
		
		Log.d(TAG, "Filtering out entries that happen before: "+new SimpleDateFormat().format(now) );
		
		int count = _maxItems;
		
		List<ScheduleEntry> schedule = ScheduleStore.INSTANCE.getAll();

        Collections.sort(schedule, new Comparator<ScheduleEntry>(){    	   
            public int compare(ScheduleEntry lhs, ScheduleEntry rhs) {
            	return lhs.getDepartureTime().compareTo(rhs.getDepartureTime());
            }
 
        });
		
		Iterator<ScheduleEntry> it = schedule.iterator();
		
		while(it.hasNext() && count > 0)
		{
			ScheduleEntry se = it.next();
			
			Date departureTime = se.getDepartureTime();
			
			Log.d(TAG, "Ferry leaving at: "+ se.getDepartureTimeString() + " (compare entry ["+new SimpleDateFormat().format(departureTime)+"] )" );
			
			if (departureTime.getTime() < now.getTime())
			{
				continue;
			}
			
			if (se.getDepartureHarbourId().equals(this.getSelectedDepartureHarbour()))
			{
				count--;
				getItems().add(se);	
			}		
		}
		
		// we need to call this to notify that the data has changed
		notifyDataSetChanged();
		
	}
}
